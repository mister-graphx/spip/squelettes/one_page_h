<?php
/**
 * Plugin One Page Horizontal Layout
 * (c) 2013 Mist. GraphX
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


// Change nested level max for xdebug
if (false !== ini_get('xdebug.max_nesting_level')) {
    ini_set('xdebug.max_nesting_level', 500);
}

$debut_intertitre = "\n<h3 class=\"spip\">\n";
$fin_intertitre = "</h3>\n";


?>