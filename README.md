# OnePage H (horizontal Layout)

**Archive :  ce squelette est publié à titre éducatif pour l'utilisation du rechargement de blocs en ajax au sein d'une page.**


## SOURCES & LIBRAIRIES :


Template inspiré de PixelMess : http://pixelmess.com/

- http://www.openjs.com/scripts/events/keyboard_shortcuts/


jQuery Library + jqueryUI

Cufon :
https://github.com/sorccu/cufon/wiki/Usage
Permet la gestion des typos sans avoir a intégrer une r^gle @font-face par type de navigateur
Le texte est référencable mais pas sélectionable

Raccourcis claviers
http://www.openjs.com/scripts/events/keyboard_shortcuts/

*   jQuery.easing
*   jQuery.scrollpane
*   jQuery.localscroll
*   jQuery.mousewheel
*   jQuery.queryloader2
*   jQuery.Scrollto
*   bootstrap-scrollspy-horizontal

Dragscroll

Citycreator

jQuery Haskell

Jquery Cycle :
http://jquery.malsup.com/cycle/

jQuery Easing

## TYPOS :

marketing_script_400 :
http://www.dafont.com/marketing-script.font
La typo est chargé avec cufon, et utilisé pour les titres des panneaux


PTsans
http://www.fontsquirrel.com/fonts/PT-Sans

SilkScreen :
http://www.fontsquirrel.com/fonts/Silkscreen
Une typo genre pixelFont




## DOC :

Le squelette One Page Horizontal permet de créer un site vitrine, pour des sites qui n'ont pas beaucoup de contenu.
On peut donc imaginer l'utiliser pour une opération évenementielle, un site de commerçant (restaurateur, artisan)

Le sommaire du site devient la seule et unique page :
tout le contenu principal est affiché en une fois, et la navigation s'effectue par un effet de scroll latéral via jquery.scrollto.

Chaque rubrique crée devient un écran.

Le fond de la page change en fonction de l'heure du pc de l'internaute.


### Limitations :

- tout le contenu étant chargé a l'affichage de la première page **on ne peut utiliser ce squelette
que pour des sites n'ayant pas beaucoup de contenu**


### Multilinguisme :

Le squelette est multilingue par secteur.

Chaque rubrique du secteur de langue devient donc un écran.
Les écrans sont ajouté bout à bout pour former une ligne et donc créer l'effet de défilement du paysage.

### Personalisation :

Changement du fond de page en fonction de l'heure du navigateur de l'internaute :

s'effectue en javascript par l'ajout d'une class css sur la body segmenté par tranche horaires : morning, noon, afternoon, evening,night et general
les couleurs de fond des class sont personalisables dans les variables less du thèmes ./css/theme.less
On peut y associer une image de fond et adapter les couleurs des contenus en fonction du fond
pour qu'il reste lisible grace a l'utilisation de less et des "Nested Rules" ou rêgles imbriquées.


Images de fond des écrans :

Les images de fond des écrans sont placées dans le dossier ./img du plugin ou peuvent êtres surchargées dans le dossier /squelettes
Les images de fond doivent êtres nommées bg_ecranX.png (X : étant le numéro de l'écran).

Pour utiliser l'effet de scroll pour créer une impression de navigation dans un paysage,
On crée des images de fond d'une largeur de 2000px.

Pour plus de facilité et pour pouvoir caller correctement les fonds, on part donc d'un gabarit de x milliers de pixels et on découpe par tranche de 2000px.

### Compositions des écrans :

Chaque écran peut bénéficier d'une structure différente via le plugin compositions.

La composition par défaut (./content/rubrique.html) s'applique si aucune composition n'as été sélectionné pour la rubrique.

* composition : Rubrique - Article d'accueil : utilise le plugin Homonyme et affiche un sommaire de rubrique


### MODÈLES DISPONIBLES


<formulaire_contact|id_auteur=XX>



### Mots clef techniques :

Mots clefs installés à l'activation du squelette pour l'organisation des écrans :
Les id des mots clefs sont stockés dans les méta de configuration du squelette pour la desinstallation.
Le groupe est automatiquement masqué du public par l'ajout du critère Mot Technique via le plugin homonyme.

Accueil - Rubrique
:   Permet de placer un article sur l'écran d'accueil d'une rubrique

Accueil - Site - Article
:   Placer un article sur l'écran d'accueil du site

Accueil - Site - Une
:   Placer un article sur la une de l'écran d'accueil du site

masquer
:   Masquer les contenus utiles pour l'administration


## TRAVAUX :


VERSION 1.0.9 :

* tentative de correction du bug sur le scroll au rechargement des articles

VERSION 1.0.7 :

*   Changement de player pixplayer : arrete de jouer le morceau en cours quand on clic sur un autre
*   Correction sur la playliste : ça ouvre le menu quand on clic sur tout le bouton et pas que la flèche

*   correction sur modele/vgn_article : suppression du message "pas de document"
    TODO : proposer un logo par défaut généré automatiquement en utilisant par exemple logo du site ??

*   correction sur les styles h3 dans les article

VERSION 1.0.6 :

DATE : Sun May 19 12:37:39 2013

*   correction sur le modèle vgn_article et sur ecran-liste-rub,
    pour que les vignettes s'affichent correctement quelque soit le format (portrait ou paysage)



DATE : Sun Apr 28 11:09:34 2013

VERSION 1.0.5 :

*   pagination sur la liste des articles ./listes/liste-contenu_rubrique.html

DATE : Sun Mar 03 14:18:54 2013

*   Ajout du classement des rubriques par num titre
*   ajout d'un identifiants aux blocs ajax de type ajax=rub#ID_RUBRIQUE

DATE : Wed Feb 20 12:56:10 2013

*   création automatique des mots clefs techniques a l'installation.
*   Stockage des id dans spip_meta pour la désinstallation propre
*   Ajout des liens vers les réseaux sociaux dans la navigation du pied de page
*   Ajout de la configuration des urls vers les réseaux sociaux
*   Passage a une composition objet type rubrique pour les ecrans :
    Permet de faire varier le layout de chaque écran indépendament

*   ajout de css/extra-varables.less : on y stocke les variables du theme
    histoire de pouvoir générer celle de bootstrap avec un outil dédié


VERSION 1.0.1 :

DATE : Fri Feb 08 12:21:32 2013

*   Intégration avvec bootstrap spir-dist


------------------------------------------------------------------------------

**BUG**

------------------------------------------------------------------------------
**TODO**

------------------------------------------------------------------------------
