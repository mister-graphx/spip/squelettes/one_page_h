<?php
/**
 * Plugin Thème éditeur
 * (c) 2012 Mist. GraphX
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation du plugin et de mise à jour.
 * Vous pouvez :
 * - créer la structure SQL,
 * - insérer du pre-contenu,
 * - installer des valeurs de configuration,
 * - mettre à jour la structure SQL 
**/
function one_page_h_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();
	include_spip('one_page_h_fonctions');
	include_spip('inc/config');
	include_spip('action/editer_objet');
	
	$maj['create'] = array(
		//array('maj_tables', array('spip_xx', 'spip_xx_liens')),
		array('install_groupe_mots'),
		array('ecrire_config', array('one_page_h', array()))
	);
	#
	# $maj['1.1.0']  = array(array('sql_alter','TABLE spip_xx RENAME TO spip_yy'));
	# $maj['1.2.0']  = array(array('sql_alter','TABLE spip_xx DROP COLUMN id_auteur'));
	# $maj['1.3.0']  = array(
	#	array('sql_alter','TABLE spip_xx CHANGE numero numero int(11) default 0 NOT NULL'),
	#	array('sql_alter','TABLE spip_xx CHANGE texte petit_texte mediumtext NOT NULL default \'\''),
	# );
	# ...

	//$maj['create'] = array(array('maj_tables', array('spip_themes')));

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
	ecrire_meta($nom_meta_base_version,$version_cible);
	ecrire_meta();
}


/**
 * Fonction de désinstallation du plugin.
 * Vous devez :
 * - nettoyer toutes les données ajoutées par le plugin et son utilisation
 * - supprimer les tables et les champs créés par le plugin. 
**/
function one_page_h_vider_tables($nom_meta_base_version) {
	# quelques exemples
	# (que vous pouvez supprimer !)
	# sql_drop_table("spip_xx");
	# sql_drop_table("spip_xx_liens");
	include_spip('inc/config');
	$config = lire_config('one_page_h/mots_techniques');
	sql_delete('spip_groupes_mots', sql_in("id_groupe", array($config['groupe'])));
	sql_delete('spip_mots', sql_in("id_groupe", array($config['groupe'])));
	
	//sql_drop_table("spip_themes");
	//
	//# Nettoyer les versionnages et forums
	//sql_delete("spip_versions",              sql_in("objet", array('theme')));
	//sql_delete("spip_versions_fragments",    sql_in("objet", array('theme')));
	//sql_delete("spip_forum",                 sql_in("objet", array('theme')));
	effacer_meta('one_page_h');
	effacer_meta($nom_meta_base_version);
	ecrire_meta();
}

?>