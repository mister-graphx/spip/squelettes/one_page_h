<?php
// Fichier faq : utilisé avec le plugin Manuel du site
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    // F
    'formulaire_contact_q'=>'Comment inserrer un formulaire de contact, dans un article',
    'formulaire_contact'=>'
Inssérer ce code dans le texte de l\'article en précisant en paramètre le numéro (ID) de l\'auteur qui souhaite recevoir les messages

<code>
<formulaire_contact|id_auteur=XX>
</code>
',
    // B
    'bloc_multi_q'=>'Inserrer des blocs multilingues',
    'bloc_multi'=>'
Pour pouvoir traduire certains champs ou objets spip, comme le descriptif du site par exemple,
on peut avoir reccours aux blocs <code><multi></code>.
<code>
<multi>
[fr]Ici le texte en français
[en]Here the english one
</multi>
</code>
',

    
    // I
    'insserer_player_q'	=> 'Comment inssérer un player mp3, dans un article',
    'insserer_player'=>'   
<code>
<doc378|player|player=pixplayer>
<doc378|player|player=neoplayer>
<doc378|player|player=dewplayer>
<doc378|player|player=eraplayer>
</code>
',
    'player_footer_q'	=> 'Fonctionement du player présent sur toutes les pages',
    'player_footer'=>'   
Le player qui est présent sur toutes les pages du site,
choisi 10 morceaux au hazard dans tous les documents sonores du site à chaque rechargement de la page.

On limite a 10 :
*- pour ne pas ralentir le chargement inutilement en chargeant 3 ou quatres albums ;-).
*- pour faire découvrir des morceaux car les utilisateurs ont tendance a écouter les mêmes tous le temps ^^.


La navigation étant prévue pour ne pas recharger la page, à chaque clic,
le son du player n\'est pas coupé lors de la visualisation des articles et diaporamas.

{{Limitations}}
Néanmoins sur certains changement d\'écrans ou rubriques le rechargement est necessaire et le son peut alors être intérrompu.
'
);

?>