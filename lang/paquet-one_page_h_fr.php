<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'one_page_h_description' => 'Squelette de site une page pour spip',
	'one_page_h_nom' => 'One Page Horizontal Layout',
	'one_page_h_slogan' => 'Une page pour tout dire !',
);

?>