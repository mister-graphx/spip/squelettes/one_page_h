<?php


/**
  *  Fichier de langue de la partie publique du site
  *  les trad de la configuration, du paquet et de l'espace privé sont dans
  *  ./one_page_h_fr.php
  */

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
    // K
    'keyboard_title'	=> 'Navigation au clavier',
    'keyboard_info'	=> 'Pressez <kbd>Shift</kbd> + <kbd>Ctrl</kbd> pour activer la navigation avec votre clavier.

Puis déplacez vous en utilisant les touches <kbd><i class="icon-arrow-left"></i></kbd> ou <kbd><i class="icon-arrow-right"></i></kbd>',


    
    // F
    'follow_me_facebook' => 'Suivez moi sur Facebook',
    'follow_me_twitter'	=> 'Suivez moi sur Twitter',
    'follow_me_viadeo'	=> 'Suivez moi sur Viadeo',

);

?>