<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
    // C
    'cfg_sommaire'	=> 'Affichage du sommaire',

    // E
    'explication_facebook'	=> 'Lien vers votre page, groupe ou profil facebook',
    'explication_sommaire'	=> 'Choisissez un type de présentation pour votre sommaire',

    'explication_home_liste_ecrans'	=> 'Composition du sommaire affichant la liste des rubrique du secteur avec leur logo sous forme de vignette',
    'explication_home_selection_articles'	=> 'Composition permettant de sélectionner les articles du sommaire, via les mots-clefs techniques',

    
    // L
    'label_facebook'	=> 'Suivez Moi sur Facebook',
    'label_sommaire'	=> 'Type de sommaire',

    'label_home_liste_ecrans'	=> 'Liste des écrans',
    'label_home_selection_articles'	=> 'Sélection d\'articles',



    
    // O
    'one_page_h_titre' => 'One Page Horizontal Layout',
    
    // T
    'titre_page_configurer_one_page_h' => 'Configuration de One Page Horizontal Layout',
);

?>