<?php
/**
 * Plugin One Page Horizontal Layout
 * (c) 2013 Mist. GraphX
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/*
 * function install_groupe_mot
 * install le groupe de mots technique et les mots clefs
 * @param 
 */

function install_groupe_mots() {
    $id_groupe_mot = sql_insertq('spip_groupes_mots',array('titre'=>'Mots Techniques','technique'=>'oui'));
    $accueil_rub = objet_inserer('mot',$id_groupe_mot);
    objet_modifier('mot',$accueil_rub,array('titre'=>'Accueil - Rubrique'));
    $accueil_site = objet_inserer('mot',$id_groupe_mot);
    objet_modifier('mot',$accueil_site,array('titre'=>'Accueil - Site - Article'));
    $accueil_site_une = objet_inserer('mot',$id_groupe_mot);
    objet_modifier('mot',$accueil_site_une,array('titre'=>'Accueil - Site - Une'));
    $masquer = objet_inserer('mot',$id_groupe_mot);
    objet_modifier('mot',$masquer,array('titre'=>'masquer'));
    
    $result = array(
        'groupe'=>$id_groupe_mot, 
        'mots'=> array(
            'accueil_rub'=>$accueil_rub,
            'accueil_site_art'=>$accueil_site,
            'accueil_site_une'=>$accueil_site_une,
            'masquer'=>$masquer
            )
    );
    
    ecrire_config('one_page_h/mots_techniques',$result);
    return $result;
}


?>