// Les fonctions createCookie() et readCookie()
// source plugin boutons_admin_supp
// ont ete recuperees depuis <http://www.quirksmode.org/js/cookies.html>
// dans le respect des droits de reproduction declares sur
// <http://www.quirksmode.org/about/copyright.html> (utilisation libre)

function createCookie(name,value,days) {
        if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
}

function getWidth() {
  var myWidth = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
  }
  return myWidth = myWidth+'px';
}

function getHeight(str) {
  var myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
	myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
	myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
	myHeight = document.body.clientHeight;
  }
  myHeight = myHeight - str;
  return myHeight = myHeight+'px';
}

/**
 * Navigation au Clavier
 *
 * ETAT : En test
 * 
 * http://www.openjs.com/scripts/events/keyboard_shortcuts/
 * Version : 2.01.B
 * By Binny V A
 * License : BSD
 */
shortcut = {
	'all_shortcuts':{},//All the shortcuts are stored in this array
	'add': function(shortcut_combination,callback,opt) {
		//Provide a set of default options
		var default_options = {
			'type':'keydown',
			'propagate':false,
			'disable_in_input':false,
			'target':document,
			'keycode':false
		}
		if(!opt) opt = default_options;
		else {
			for(var dfo in default_options) {
				if(typeof opt[dfo] == 'undefined') opt[dfo] = default_options[dfo];
			}
		}

		var ele = opt.target;
		if(typeof opt.target == 'string') ele = document.getElementById(opt.target);
		var ths = this;
		shortcut_combination = shortcut_combination.toLowerCase();

		//The function to be called at keypress
		var func = function(e) {
			e = e || window.event;
			
			if(opt['disable_in_input']) { //Don't enable shortcut keys in Input, Textarea fields
				var element;
				if(e.target) element=e.target;
				else if(e.srcElement) element=e.srcElement;
				if(element.nodeType==3) element=element.parentNode;

				if(element.tagName == 'INPUT' || element.tagName == 'TEXTAREA') return;
			}
	
			//Find Which key is pressed
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			var character = String.fromCharCode(code).toLowerCase();
			
			if(code == 188) character=","; //If the user presses , when the type is onkeydown
			if(code == 190) character="."; //If the user presses , when the type is onkeydown

			var keys = shortcut_combination.split("+");
			//Key Pressed - counts the number of valid keypresses - if it is same as the number of keys, the shortcut function is invoked
			var kp = 0;
			
			//Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
			var shift_nums = {
				"`":"~",
				"1":"!",
				"2":"@",
				"3":"#",
				"4":"$",
				"5":"%",
				"6":"^",
				"7":"&",
				"8":"*",
				"9":"(",
				"0":")",
				"-":"_",
				"=":"+",
				";":":",
				"'":"\"",
				",":"<",
				".":">",
				"/":"?",
				"\\":"|"
			}
			//Special Keys - and their codes
			var special_keys = {
				'esc':27,
				'escape':27,
				'tab':9,
				'space':32,
				'return':13,
				'enter':13,
				'backspace':8,
	
				'scrolllock':145,
				'scroll_lock':145,
				'scroll':145,
				'capslock':20,
				'caps_lock':20,
				'caps':20,
				'numlock':144,
				'num_lock':144,
				'num':144,
				
				'pause':19,
				'break':19,
				
				'insert':45,
				'home':36,
				'delete':46,
				'end':35,
				
				'pageup':33,
				'page_up':33,
				'pu':33,
	
				'pagedown':34,
				'page_down':34,
				'pd':34,
	
				'left':37,
				'up':38,
				'right':39,
				'down':40,
	
				'f1':112,
				'f2':113,
				'f3':114,
				'f4':115,
				'f5':116,
				'f6':117,
				'f7':118,
				'f8':119,
				'f9':120,
				'f10':121,
				'f11':122,
				'f12':123
			}
	
			var modifiers = { 
				shift: { wanted:false, pressed:false},
				ctrl : { wanted:false, pressed:false},
				alt  : { wanted:false, pressed:false},
				meta : { wanted:false, pressed:false}	//Meta is Mac specific
			};
                        
			if(e.ctrlKey)	modifiers.ctrl.pressed = true;
			if(e.shiftKey)	modifiers.shift.pressed = true;
			if(e.altKey)	modifiers.alt.pressed = true;
			if(e.metaKey)   modifiers.meta.pressed = true;
                        
			for(var i=0; k=keys[i],i<keys.length; i++) {
				//Modifiers
				if(k == 'ctrl' || k == 'control') {
					kp++;
					modifiers.ctrl.wanted = true;

				} else if(k == 'shift') {
					kp++;
					modifiers.shift.wanted = true;

				} else if(k == 'alt') {
					kp++;
					modifiers.alt.wanted = true;
				} else if(k == 'meta') {
					kp++;
					modifiers.meta.wanted = true;
				} else if(k.length > 1) { //If it is a special key
					if(special_keys[k] == code) kp++;
					
				} else if(opt['keycode']) {
					if(opt['keycode'] == code) kp++;

				} else { //The special keys did not match
					if(character == k) kp++;
					else {
						if(shift_nums[character] && e.shiftKey) { //Stupid Shift key bug created by using lowercase
							character = shift_nums[character]; 
							if(character == k) kp++;
						}
					}
				}
			}
			
			if(kp == keys.length && 
						modifiers.ctrl.pressed == modifiers.ctrl.wanted &&
						modifiers.shift.pressed == modifiers.shift.wanted &&
						modifiers.alt.pressed == modifiers.alt.wanted &&
						modifiers.meta.pressed == modifiers.meta.wanted) {
				callback(e);
	
				if(!opt['propagate']) { //Stop the event
					//e.cancelBubble is supported by IE - this will kill the bubbling process.
					e.cancelBubble = true;
					e.returnValue = false;
	
					//e.stopPropagation works in Firefox.
					if (e.stopPropagation) {
						e.stopPropagation();
						e.preventDefault();
					}
					return false;
				}
			}
		}
		this.all_shortcuts[shortcut_combination] = {
			'callback':func, 
			'target':ele, 
			'event': opt['type']
		};
		//Attach the function with the event
		if(ele.addEventListener) ele.addEventListener(opt['type'], func, false);
		else if(ele.attachEvent) ele.attachEvent('on'+opt['type'], func);
		else ele['on'+opt['type']] = func;
	},

	//Remove the shortcut - just specify the shortcut and I will remove the binding
	'remove':function(shortcut_combination) {
		shortcut_combination = shortcut_combination.toLowerCase();
		var binding = this.all_shortcuts[shortcut_combination];
		delete(this.all_shortcuts[shortcut_combination])
		if(!binding) return;
		var type = binding['event'];
		var ele = binding['target'];
		var callback = binding['callback'];

		if(ele.detachEvent) ele.detachEvent('on'+type, callback);
		else if(ele.removeEventListener) ele.removeEventListener(type, callback, false);
		else ele['on'+type] = false;
	}
}


/**
 * lazyLoad : lance le pré-chargeent des images
 *
 * http://www.gayadesign.com/diy/queryloader2-preload-your-images-with-ease/
 * https://github.com/Gaya/QueryLoader2
 * 
 * @param 
 */

function lazyLoad() {
    // QueryLoad : préchargement des images et scripts
    $("body").queryLoader2({
        //barColor: "#6e6d73",
        //backgroundColor: "#fff1b0",
        percentage: true,
        barHeight: 1,
        completeAnimation: "grow",
        minimumTime: 100
    }); 
}


/**
 * TimeChange : changement du background en fonction de l'heure
 * 
 * // change background depending on user's time :
 * // http://forum.jquery.com/topic/jquery-change-background-based-of-time-of-day-using-jquery
 * 
 * @param 
 */

function TimeChange() {
    datetoday = new Date();
    timenow = datetoday.getTime();
    datetoday.setTime(timenow);
    thehour = datetoday.getHours();
    if (thehour >= 18)
    $('body').addClass('evening');
    else if (thehour >= 15)
    $('body').addClass('afternoon');
    else if (thehour >= 12)
    $('body').addClass('noon');
    else if (thehour >= 7)
    $('body').addClass('morning');
    else if (thehour >= 0)
    $('body').addClass('night');
    else
    $('body').addClass('general');	
}

/**
 *  switchClass
 *
 *  enlève une class css et la remplace par une autre dans un selecteur
 *
 *  @param : selecteur : #id
 *  @param : removed : la classe a supprimer
 *  @param : replaced : la classe a ajouter
*/
function switchClass(selecteur,removed,replaced){
	$(selecteur).removeClass(removed);
	$(selecteur).addClass(replaced);
}


/**
 * fonction AllerA
 * Gestion des liens internes entre les écrans
 * avec chargement de l'article a consulter
 * @param ancre {String} ancre nommée
 * @param id_article {Integer} id de l'article
*/
function AllerA(id_rubrique,id_article) {
	var ancre = '#screen' + id_rubrique;
	
	$('#content-wrapper').stop().scrollTo($(ancre), 1500, {
	    easing:'easeOutExpo',
	    onAfter:function(){
		//url =  parametre_url(window.location.href,'voir','oui');
		//url =  parametre_url(url,'id_article',id_article);
		//url =  parametre_url(url,'ecran',id_rubrique);
		//window.location.href = url;
		var bloc = '#contenu' + id_rubrique;
		$(bloc).ajaxReload({
			args:{
				voir:'oui',
				id_article:id_article,
				ecran:id_rubrique
			}
		});
		}
	    });
}

/**
 * fonction scroll
 * Gestion des liens inetrnes entre les écrans
 * on glisse d'un lien vers l'écran de destination
 * Utilisé pricipalement sur le sommaire avec les liens entre écrans
 * 
 * @param ancre {String} ancre nommée
 * 
*/
function scroll(ancre) {
    $('#content-wrapper').stop().scrollTo($('#' + ancre), 1500, {
	easing:'easeOutExpo'
    });
}

// Recharge/ré-inittialise jScroll sur une action
function scrollpaneReload(ajaxbloc,id_article,id_rubrique){
    var settings = {
        showArrows: true
    };
    var pane = $(ajaxbloc);
    pane.jScrollPane(settings);
    var api = pane.data('jsp');
    
    //var originalContent = api.getContentPane().html();
    //api.getContentPane().html(originalContent + 'COUCOU !!');
    //
    //console.log('Ré-initialisation !!!' + ajaxbloc.id );
    
    ajaxReload(ajaxbloc.id,{
            callback:function(){
                //var originalContent = api.getContentPane().html();
                //api.getContentPane().html(originalContent + originalContent + originalContent);
                //console.log('Ré-initialisation !!! du bloc' + ajaxbloc.id);
                //alert('CallBack !!');
                api.reinitialise();
            },
            args:{
                voir:'oui',
                id_article:id_article,
                ecran:id_rubrique
            },
            history:false
    });
}



// MARK : Dom Ready
jQuery(function($){
	
	// Activation d'un lien au clic sur un div
	$(".item").click(function(){
	    window.location = $(this).find("a").eq(0).attr('href');
	});
	
	$(".item").hover(function(){
	    $(this).css({'cursor':'pointer'});
	    // On peut ajouter ici d'autres modifications CSS au survol du bloc
	    },function(){
	    // Ici, annuler les modifications CSS du survol.
	    // Le curseur reprend sa forme automatiquement
	});
	
	// Cookie pour le pré-chargement
	var loading = readCookie("loading");
	if(!loading){
	      createCookie("loading",true,7);
	      lazyLoad();
	}
	
	// Ajustements de blocs du layout en fonction de la taille du navigateur
	$(".customwidth").width(getWidth());
	$("#content-wrapper").height(getHeight(40));
	$(".encart").height(getHeight(140));
	$(".jspContainer").height(getHeight(140));
	//$(".content").height(getHeight(20));
	//$(".screen").height(getHeight(95));
	
	$(window).resize(function() {
		$(".customwidth").width(getWidth());
		$("#content-wrapper").height(getHeight(40));
		$(".encart").height(getHeight(140));
		$(".jspContainer").height(getHeight(140));
		//$(".screen").height(getHeight(95));
		//$('#sky').height(getHeight(300));
		//$('#console-container').width(getWidth()).height(getHeight(30));
	});
	
	// 
	var WrapperHeight = $('#content-wrapper').height();
	var ContainerHeight = $('.contenu-ecran').height();
	if(ContainerHeight>WrapperHeight){
		// Debug
		//alert('Le Wrapper est inférieur a 500px : '+WrapperHeight+'Container => '+ContainerHeight);
		//$('.contenu-ecran').jScrollPane({});
	}
	    // Le contenu étant plus grand que le Wrapper : on affiche un barre de défilement
	//     	$('.contenu-ecran').each(function(){
	//		$(this).jScrollPane({
	//				showArrows: $(this).is('.arrow')
	//			}
	//		);
	//		var api = $(this).data('jsp');
	//		var throttleTimeout;
	//		$(window).bind(
	//			'resize',
	//			function(){
	//			    if ($.browser.msie) {
	//					// IE fires multiple resize events while you are dragging the browser window which
	//					// causes it to crash if you try to update the scrollpane on every one. So we need
	//					// to throttle it to fire a maximum of once every 50 milliseconds...
	//					if (!throttleTimeout) {
	//						throttleTimeout = setTimeout(
	//							function()
	//							{
	//								api.reinitialise();
	//								throttleTimeout = null;
	//							},
	//							50
	//						);
	//					}
	//			    } else {
	//				    api.reinitialise();
	//			    }
	//			}
	//		);
	//	});
	    //alert('Le Wrapper est inférieur a 500px : '+WrapperHeight+'Container => '+ContainerHeight);
	//}
	
        //$('#sommaire').swipe({
        //    //Generic swipe handler for all directions
        //    swipe:function(event, direction, distance, duration, fingerCount) {
        //      $(this).text("You swiped " + direction );  
        //    },
        //    //Default is 75px, set to 0 for demo so any distance triggers swipe
        //     threshold:0
        //});
        
        
        


});