<?php
/**
 * Skel One_page_h
 * (c) 2012 Mist. GraphX
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 */
function one_page_h_declarer_tables_interfaces($interfaces) {

	//$interfaces['table_des_tables']['themes'] = 'themes';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 */
function one_page_h_declarer_tables_objets_sql($tables) {

	//$tables['spip_themes'] = array(
	//	'type' => 'theme',
	//	'principale' => "oui",
	//	'field'=> array(
	//		"id_theme"           => "bigint(21) NOT NULL",
	//		"id_rubrique"        => "bigint(21) NOT NULL DEFAULT 0", 
	//		"id_secteur"         => "bigint(21) NOT NULL DEFAULT 0",
	//		"titre"              => "text NOT NULL DEFAULT ''",
	//		"styles"             => "text NOT NULL DEFAULT ''",
	//		"maj"                => "TIMESTAMP"
	//	),
	//	'key' => array(
	//		"PRIMARY KEY"        => "id_theme",
	//		"KEY id_rubrique"    => "id_rubrique", 
	//		"KEY id_secteur"     => "id_secteur", 
	//	),
	//	'titre' => "'' AS titre, '' AS lang",
	//	 #'date' => "",
	//	'champs_editables'  => array(
	//		"titre","id_rubrique","id_secteur","styles"),
	//	'champs_versionnes' => array(),
	//	'rechercher_champs' => array(),
	//	'tables_jointures'  => array(),
	//	
	//
	//);

	return $tables;
}



?>